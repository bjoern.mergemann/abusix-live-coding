
// TASKS
// 1: You need an array of IDs (as integers).
// 2: Restructure the data to have first- and lastname as the key and id as the value.

// 5, 23, 42, 91 and 107 are the users ids. Backend-Dev had a very bad day.
const data = {
    5: {
        firstname: 'Frederik',
        lastname: 'Petersen',
    },
    23: {
        firstname: 'Christin',
        lastname: 'Mattuschka',
    },
    42: {
        firstname: 'Tobias',
        lastname: 'Knecht',
    },
    91: {
        firstname: 'Alexander',
        lastname: 'Wagner',
    },
    107: {
        firstname: 'Björn',
        lastname: 'Mergemann',
    },
};
