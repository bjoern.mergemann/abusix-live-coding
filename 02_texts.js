// TASK
// 01: Think about a function to count the appearances of a single word in this text snippet
// 02: Reverse the whole string (without using .reverse())

const snippet = `Well, the way they make shows is, they make one show. That show's called a pilot. Then they show that show to the people who make shows, and on the strength of that one show they decide if they're going to make more shows. Some pilots get picked and become television programs. Some don't, become nothing. She starred in one of the ones that became nothing.`;
